package domain;

public class Person {
	private String first_name;
	private String last_name;
	private PersonType type;
	
	public void setFirstName(String name){
		this.first_name = name;
	}
	public String getFirstName(){
		return this.first_name;
	}

	public void setLastName(String name){
		this.last_name = name;
	}
	
	public String getLastName(){
		return this.last_name;
	}
	
	public void setType(PersonType type){
		this.type = type;
	}
	
	public PersonType getType(){
		return this.type;
	}
}
