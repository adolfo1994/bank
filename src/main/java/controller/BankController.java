package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import domain.Account;
import repository.AccountRepository;
import repository.InMemoryRespotoryFactory;
import repository.RepositoryFactory;
@SessionAttributes("accountsRepo")
@Controller
public class BankController {
	@RequestMapping("/bank")
	String render(ModelMap model, HttpServletRequest request) {
		RepositoryFactory factory = new InMemoryRespotoryFactory();
		AccountRepository accountsRepo = factory.createAccountRepository();
		Account a1 = new Account("1000", 500);
		Account a2 = new Account("1001", 5000);
		accountsRepo.save(a1);
		accountsRepo.save(a2);
		HttpSession session = request.getSession();
		session.setAttribute("accountsRepo", accountsRepo);
		return "index";
	}
}
