package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import repository.AccountRepository;
import service.TransferService;

@SessionAttributes("accountsRepo")
@Controller
public class TransferController {
	
	@RequestMapping("/transferForm")
	String render(ModelMap model, HttpServletRequest request){
		HttpSession session = request.getSession();
		AccountRepository accountsRepo = (AccountRepository) session.getAttribute("accountsRepo");
		model.addAttribute("accounts", accountsRepo);
		return "transferForm";
	}
	@ResponseBody
	@RequestMapping("/doTransfer")
	String execute(HttpServletRequest request,
			HttpServletResponse response,
					@RequestParam(value = "source", required=false) String source,
					@RequestParam(value = "target", required=false) String target, 
					@RequestParam(value = "ammount", required=false) Integer ammount){
		HttpSession session = request.getSession();
		AccountRepository accountsRepo = (AccountRepository) session.getAttribute("accountsRepo");
		System.out.println(accountsRepo);
		response.setContentType("text/plain");
		response.setCharacterEncoding("utf8");
		
		if(source == null || target == null || ammount == null ){
			return "Failure";
		} 
		TransferService service = new TransferService(accountsRepo);
		String mySource = source;
		String myTarget = target;
		Integer amm = ammount;
		if (service.transfer(mySource, myTarget, amm)){
			return "Success";
		}else{
			return "Failure";
		}
	}
}
