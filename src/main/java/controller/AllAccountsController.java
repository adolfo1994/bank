package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import repository.AccountRepository;
import repository.InMemoryRespotoryFactory;
import repository.RepositoryFactory;
import domain.Account;

@SessionAttributes("accountsRepo")
@Controller
public class AllAccountsController {
	
	@RequestMapping("/showAccounts")
	String render(ModelMap model, HttpServletRequest request){
		HttpSession session = request.getSession();
		AccountRepository accountsRepo = (AccountRepository) session.getAttribute("accountsRepo");
		List<Account> accounts = accountsRepo.findAll();
		model.addAttribute("accounts", accounts);
		return "allAccounts";
	}
}
