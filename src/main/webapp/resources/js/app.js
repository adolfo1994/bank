var app = angular.module('BankApp', ['ngMaterial'])
.config(function($mdThemingProvider){
	$mdThemingProvider.theme('default')
	.primaryPalette('blue');
});

app.controller('AppCtrl', ['$scope', '$mdSidenav','$http', '$compile', '$element', '$document',
                           '$mdToast', '$animate',
                           function($scope, $mdSidenav, $http, $compile, $element, $document, $mdToast, $animate){
	$scope.Services = [
	                   {
	                	   name: 'Show Accounts',
	                	   contentUrl: '/showAccounts'
	                	   
	                   },
	                   {
	                	   name: 'Transfer',
	                	   contentUrl: '/transferForm'
	                	   
	                   }
	                   ];
	$scope.transferUrl = "/doTransfer"
	
	$scope.toggleSidenav = function(menuId) {
		$mdSidenav(menuId).toggle();
  };
  
  $scope.showToast = function(type, msg){
		    $mdToast.show({
		        template: '<md-toast class="md-toast ' + type +'">' + msg + '</md-toast>',
		        hideDelay: 3000,
		        position: $scope.getToastPosition()
		    });
		};

  	$scope.loadContent = function(contentUrl){
  		$http.get(contentUrl, { headers: { 'Content-Type' : 'application/html' } }).success(function(data, code){
  			var $e = angular.element(document.querySelector('#myContent'));
  	  		var newStuff = $e.html(data);
  	  		var scope = $scope;
  	  		$compile(newStuff)(scope);
  	  		});
  		$scope.toggleSidenav('left');
  		};
  	
  	$scope.toastPosition = {
  			bottom: true,
            top: false,
            left: false,
            right: true
        };

     $scope.getToastPosition = function() {
    	 return Object.keys($scope.toastPosition)
                .filter(function(pos) { return $scope.toastPosition[pos]; })
                .join(' ');
        };

        $scope.doTransfer = function() {
        	
        	var req = {
        			 method: 'POST',
        			 url: $scope.transferUrl,
        			 headers: {
        			   'Content-Type': 'application/x-www-form-urlencoded'
        			 },
        			 params: {
        	        		source: $scope.sourceAccount,
        	        		target: $scope.targetAccount,
        	        		ammount: $scope.ammount
        	        	},
        			 transformResponse: undefined
        			}
        	
        	$http(req).success(function(data, status, headers, config){
        		console.log(data);
        		
        		if(data == "Success"){
        			$scope.showToast("success", "The transfer was succesfull, please check on 'Show Accounts Tab'."  );
        		}else{
        			$scope.showToast("error", "There was an error during your transfer, please try again");
        		}
        	});
        
        };
 
}]);